extends Label

func _ready():
	hide()

func _process(delta):
	if (get_tree().paused):
		show()
	else:
		hide()
