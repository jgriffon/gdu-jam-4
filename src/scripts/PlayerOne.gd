extends Player


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_Queue_gui_input(event):
	if self._isMyTurn:
		return
	if event is InputEventMouseButton:
		GameManager.emit_signal("show_queue", self)
