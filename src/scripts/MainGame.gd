extends ColorRect


# Current "deck" for this game
var gameBlocks: Array

var currentPlayer
var _force_winner

var players = []

var _remove_blocks = 4
var _players_out = 0
var point_block = 32500


onready var _turnDelay = $TurnDelay

func _enter_tree():
	# Assign players
	players = [
		$PlayerOne/PlayerOne,
		$PlayerTwo/PlayerTwo
	]
	
	GameManager.player_character = players[1]
	
	# Initialise the data
	# Load the effects
	GameManager._effects._game_impl = self
	GameManager._load_cards()

	# Subscribe to the signals
	GameManager.connect("game_start", self, "_on_game_start", [], CONNECT_DEFERRED)
	GameManager.connect("game_win", self, "_on_game_win", [], CONNECT_DEFERRED)
	GameManager.connect("round_start", self, "_on_round_start", [], CONNECT_DEFERRED)
	GameManager.connect("round_end", self, "_on_round_end", [], CONNECT_DEFERRED)
	GameManager.connect("turn_end", self, "_on_turn_end", [], CONNECT_DEFERRED)
	GameManager.connect("game_end", self, "_on_game_end", [], CONNECT_DEFERRED)	
	GameManager.connect("queue_empty", self, "_on_queue_empty", [], CONNECT_DEFERRED)
	GameManager.connect("show_queue", self, "_on_show_queue", [], CONNECT_DEFERRED)

	
func _on_game_start():
	# Do all the prep work and then start the round!
	var randomPlayer = 0
	
	# Determine who goes first
	randomize()
	randomPlayer = randi()%2
	
	assert(randomPlayer < 2)
	
	# Assign random player to be the first player
	currentPlayer = randomPlayer
	
	# Begin the first round
	GameManager.emit_signal("round_start")

func _on_game_win(player):
	print("game_win")
	# Display the win text
	$WinDisplay/Label.text = player.display_name + " wins!"
	$WinDisplay.show()
	yield(get_tree().create_timer(5.0), "timeout")
	$WinDisplay.hide()
	GameManager.emit_signal("game_end")
	
func _on_round_start():
	gameBlocks = GameManager.shuffle(GameManager.deck)
	
	assert(gameBlocks.size() == GameManager.total_cards)
	
	# Remove the requisite blocks
	for i in _remove_blocks:
		gameBlocks.pop_front()
	
	assert(gameBlocks.size() == (GameManager.total_cards - 4))
	
	#Set up the first player
	players[currentPlayer].yourTurn()
	
	# Deal a block out
	$PlayerOne/PlayerOne.queueBlock(gameBlocks.pop_front())
	$PlayerTwo/PlayerTwo.queueBlock(gameBlocks.pop_front())

	# Draw a block for the current player
	players[currentPlayer].queueBlock(gameBlocks.pop_front())
	
	# Begin the turn
	GameManager.emit_signal("turn_start")
	
func switch_player(): 
	currentPlayer = other_player()

func other_player() -> int:
	var curr = currentPlayer + 1
	if (curr == players.size()):
		curr = 0
	
	return curr
func _on_turn_end():
	# wait a second
	_turnDelay.start()

func _on_TurnDelay_timeout():		
	# Switch the player 
	switch_player()
	
	# Queue the next block, if available
	if (gameBlocks.size() > 0):
		players[currentPlayer].queueBlock(gameBlocks.pop_front())
	
	# Enable the next player 
	players[currentPlayer].yourTurn()
	
	# Start the next turn
	GameManager.emit_signal("turn_start")

func _on_round_end():
	get_tree().paused = true
	GameManager.set_block_signals(true)
	
	# Determine who won
	var winner = players[0]
	
	# Override optional
	if (_force_winner != null):
		winner = _force_winner
	else:
		for player in players:
			if player.get_live_block() > winner.get_live_block():
					winner = player

	# Add the points to the winning player
	winner.add_points(point_block)
	# Tell the player
	GameManager.display_text( winner.display_name + " just gained " + 
							winner._point_format % (point_block / winner._point_scale))
	# Change the progress bar
	$ProgressBar.set_balance(players, GameManager.points_goal)

	# Display the points results
	for player in players:
		player._pointNode.show()
	
	# Wait and hide the point results
	$RoundDelay.start()
		
func _on_queue_empty():
	_players_out += 1
	if _players_out == players.size():
		GameManager.emit_signal("round_end")

func _on_RoundDelay_timeout():
	# For debugging
	print_debug("RoundDelay timer expired!!")
	# Reset the player counts
	_players_out = 0
	
	# Hide the point displays and clear the field
	for player in players:
		player._pointNode.hide()
		player.clear_blocks()
	
	# Check point totals
	for player in players:
		if(player._points >= GameManager.points_goal):
			GameManager.set_block_signals(false)
			GameManager.emit_signal("game_win", player)
			return 			
	# Call a new round
	get_tree().paused = false
	GameManager.set_block_signals(false)
	GameManager.emit_signal("round_start")

func _on_game_end():
	# Reload the title screen
	var title = GameManager._titleScreen.instance()

	# Switch scenes
	get_tree().get_root().add_child(title)
	
	# Cleanup extant blocks
	for player in players:
		player.clear_blocks()
		# reset flags
		player.reset_flags()

	# Unubscribe to the signals
	GameManager.disconnect("game_start", self, "_on_game_start")
	GameManager.disconnect("game_win", self, "_on_game_win")
	GameManager.disconnect("round_start", self, "_on_round_start")
	GameManager.disconnect("round_end", self, "_on_round_end")
	GameManager.disconnect("turn_end", self, "_on_turn_end")
	GameManager.disconnect("game_end", self, "_on_game_end")	
	GameManager.disconnect("queue_empty", self, "_on_queue_empty")
	GameManager.disconnect("show_queue", self, "_on_show_queue")
	
	GameManager._effects._game_impl = null
	
	get_tree().get_root().remove_child(self)
	
func _on_show_queue(player):
	$QueueDisplay.show_blocks(player)
	
func display_text(text):
	get_tree().paused = true
	$InfoDisplay/Text.text = text
	$InfoDisplay.show()
	# Wait for a couple seconds 
	yield(get_tree().create_timer(0.25), "timeout")
	
	yield(get_tree().create_timer(1), "timeout")
	get_tree().paused = false
	$InfoDisplay.hide()
	
