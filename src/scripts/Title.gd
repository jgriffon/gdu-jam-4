extends ColorRect

var _game = preload("res://scenes/MainGame.tscn").instance()

func _ready():
	pass
	
func _on_Button_pressed():
	$TitleAnimation.play("start_game")

func _on_TitleAnimation_animation_finished(anim_name):
	if (anim_name == "start_game"):
			get_tree().get_root().add_child(_game)
			get_tree().get_root().remove_child(self)
			
	GameManager.emit_signal("game_start")

func _on_QuitButton_pressed():
	# Send the quit notification
	get_tree().quit()
