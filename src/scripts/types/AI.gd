extends Node
class_name AI

func play_auto(line_up):
	var i
	# Dumb AI: choose card at random to play
	randomize()
	i = randi() % line_up.size()
	
	print("thinking...")
	# Wait so it looks like  something is happening
	yield(GameManager._effects._game_impl.get_tree().create_timer(2.0),
	"timeout")
	print("play")
	# Play the block
	GameManager.emit_signal("play_block", line_up[i])
	
func choose_alithia(modal_object):
	var i
	var blocks = modal_object.get_node("Margin/Contents/Blocks").get_children()
	var block_count = modal_object.get_node("Margin/Contents/Blocks").get_child_count()
	
	# First choose one of the blocks at random
	randomize()
	i = randi() % block_count
	
	# Mark this as the selected block
	modal_object._pressed = blocks[i]

	# "Press" the confirm button
	modal_object._on_Confirm_pressed()
	
	
