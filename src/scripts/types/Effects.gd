extends Node
class_name Effects

var _game_impl
var 	alithia_modal = preload("res://scenes/effects/Alithia.tscn")
enum Target {
	PLAYER_CHOOSE,
	PLAYER_SELF,
	PLAYER_OPPONENT
}

func _init():
	pass
	
func _on_effect_alithia(target):
	var opponent = _get_target(target)
	var player = _get_player()
	var box
	
	if (opponent.block_alithia ||
		opponent.block_effects):
			opponent.block_count += 1
			# Tell the player
			GameManager.display_text( "Seamus was seen by a staff member! No effect.")
			return
	# Pause the game
	_game_impl.get_tree().paused = true
	
	# Instance the modal
	box = alithia_modal.instance()
	_game_impl.add_child(box)
	
	# Load opponent info, so it's not null
	box.emit_signal("player_info", opponent)
	
	# Check if a human player
	if (player.is_human):
		# Load the dialog box for choosing a block
		box.show()
	else:
		# Refer decision back to the AI
		player._ai.choose_alithia(box)

func _on_effect_six(target):
	# Pause 
	_game_impl.get_tree().paused = true
	
	var player = _get_target(target)
	var block
	
	# If this action is blocked, return
	if (player.block_effects):
		player.block_count += 1
		# Tell the player
		GameManager.display_text( "Unfortunately, The Six hit its lowest ratings ever.\n" + player.display_name + " was spared an embarassing exposé.")
		_game_impl.get_tree().paused = false
		return
	
	# Say what's happening
	GameManager.display_text(player.display_name + " suffered negative press from The Six and loses their queue.")	
	# Empty the player's queue
	player._queue = []
	player._clear_queue()
	
	# Add a new block to the queue
	block = _game_impl.gameBlocks.pop_front()
	player.queueBlock(block)
	
	# Unpause
	_game_impl.get_tree().paused = false
	
func _on_effect_show_queue(target):
	var player = _get_target(target)
	var curr_player = _get_player()
	
	if (!player.block_effects):
		# Check to see if human first
		if (curr_player.is_human):
			GameManager.emit_signal("show_queue", player)
		else:
			# TODO: implement AI learning for queue
			# curr_player._ai.learn_queue(player)
			pass
	else:
		# Tell the player, if they're human
		if (curr_player.is_human):
			GameManager.display_text( "Turns out to be a scam. There was no ad space,\n so you don't get to spy on the network.")
		player.block_count += 1

func _on_effect_flutter(target):
	var affected = _get_target(target)
	if (affected.block_effects):
		affected.block_count += 1
		# Tell the player
		GameManager.display_text( affected.display_name + 
		" has such a strong social media following,\n nothing could keep them down...")
		return

	for player in _game_impl.players:
		# Force play the other player's hand
		var prevBlock = player._liveNode.get_child(0)
		var queuedBlock = player._queueNode.get_child(0)
		
		if (queuedBlock != null):
			if(prevBlock != null):
				player._cancel_block(prevBlock)
			player._move_live(queuedBlock)
			
	GameManager.emit_signal("round_end")

func _on_effect_protect(target):
	var affected = _get_target(target)
	affected.block_effects = true
	# Tell the player
	GameManager.display_text( affected.display_name
		+ " is protected from all effects until next turn!")

func _on_effect_protect_alithia(target):
	var affected = _get_target(target)
	affected.block_alithia = true	
	# Tell the player
	GameManager.display_text( affected.display_name
		+ " is protected from Seamus O'Bleefe's antics for 1 turn")

func _on_effect_mucker(target):
	var opponent = _get_target(target)
	if(!opponent.block_effects):
		# Block all signals first
		GameManager.set_block_signals(true)
		GameManager.display_text("Mucker brings fire down, causing the broadcast day to end early.")
		yield(_game_impl.get_tree().create_timer(1.5), "timeout")
		# Resume signals
		GameManager.set_block_signals(false)
		GameManager.emit_signal("round_end")
	else:
		GameManager.display_text("Mucker was out sick for the day, and his replacement couldn't bring the heat.")
		opponent.block_count += 1
		

func _on_effect_queue(target):
	var player = _get_target(target)
	
	player.force_play_check = true
	# Check the queue for one of the affected cards
	for _block in player._queueNode.get_children():
		if (_block.iq == 5 ||
			_block.iq == 6):
			# Autoplay the block
			GameManager.emit_signal("play_block", _block)
			
# Utilities	
func _add(effect_name, target):
	GameManager.add_user_signal(effect_name, [target])
	GameManager.connect(effect_name, self, "_on_" + effect_name)

func _get_other_player():
	# Use ref to game instance to get the other player
	# This will need revamping for more than one player
	var other_player = _game_impl.other_player()
	var player = _game_impl.players[other_player]
	
	return player

func _get_target(target):
	var value = null
	
	match target as int:
		Target.PLAYER_CHOOSE:
			#TODO: Implement choosing mechanism
			value =  _get_other_player()
		Target.PLAYER_OPPONENT:
			value =  _get_other_player()
		Target.PLAYER_SELF:
			value = _get_player()

	return value

func _get_player():
	return _game_impl.players[_game_impl.currentPlayer]
