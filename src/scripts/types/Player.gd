extends HBoxContainer
class_name Player

# Player-wide signals
signal show_blocks
signal hide_blocks

## Basic Player object
export var display_name:String
export var is_human:bool = false
var _queue = []
var _lineUp = []
var _ai: AI

var _block = preload("res://scenes/Block.tscn")
var _isMyTurn: bool = false
var force_play_check = false
var block_effects = false
var block_alithia = false
var block_count = 0
var _points = 0

const _point_format = "%2.1fm viewers"
const _point_scale = 1000.0
# Node references
var _queueNode: Node
var _cancelledNode: Node
var _liveNode: Node
var _pointNode: Node

# Called when the node enters the scene tree for the first time.
func _ready():
	GameManager.connect("turn_start", self, "_on_turn_start", [], CONNECT_DEFERRED)
	GameManager.connect("round_end", self, "_on_round_end", [], CONNECT_DEFERRED)
	darken()
	
	# Grab child nodes
	_queueNode = get_node("QueueContainer/Queue/Contents")
	_liveNode = get_node("Live/Contents")
	_cancelledNode = get_node("CancelledContainer/Cancelled/Contents")
	_pointNode = get_parent().get_node("PointDisplay")
	
	# Add AI if not a human player
	if (!is_human):
		_ai = AI.new()
	# Debug
	assert(_queueNode != null)
	assert(_liveNode != null)
	assert(_cancelledNode != null)
	assert(_pointNode != null)
	
func queueBlock(block):
	var newBlock
	var pos = -1 
	#DEBUG
	print_debug("Queuing block...")
	print_debug("Queue before: " + str(_queue))
	_queue.append(block)
	print_debug("Queue after: " + str(_queue))
	pos = _queue.size() - 1
	
	#DEBUG
	print_debug("QueueNode before: " + str(_queueNode.get_child_count()))
	# Instance the block node and attach as a child
	newBlock = _block.instance()
	newBlock.iq = block
	newBlock._active = (_isMyTurn && is_human)
	newBlock._queue_pos = pos
	_queueNode.add_child(newBlock)
	
	#DEBUG
	print_debug("QueueNode after: " + str(_queueNode.get_child_count()))
	
	# Hide if not a human player
	if (!is_human):
		newBlock.hide_block()
	
	# Check if we need to check the block
	# and autoplay it
	if (force_play_check):
		if (newBlock.iq == 5 || 
			newBlock.iq == 6):
			GameManager.emit_signal("play_block", newBlock)
	
# Method for starting this player's turn
func yourTurn():
	print_debug("It's " + display_name + "'s turn")
	_isMyTurn = true
	
	# Set the blocks in the queue to active/inactive
	_toggle_queue()
	
	# Connect play signal
	GameManager.connect("play_block", self, "_on_play_block", [], CONNECT_ONESHOT)

func darken():
	set_modulate(Color(1, 1, 1, 0.5))
	
func lighten():
	set_modulate(Color(1, 1, 1, 1))
	
func _on_turn_start():
	if (_isMyTurn):
		lighten()
	else:
		darken()
	
	# Increment block counter if it's your turn
	if (_isMyTurn && 
	(block_alithia || block_effects))	:
		block_count += 1
		
	# Check the blocks
	if (block_count > 0):
		print_debug("blocks expire")
		block_alithia = false
		block_effects = false
		block_count = 0
		
	# Use AI if not human
	if (!is_human && _isMyTurn):
		_ai.play_auto(_queueNode.get_children())

# Play a block from the queue to the lineup
func _on_play_block(block):
	var prevBlock = null
	#DEBUG
	print_debug("Playing block..." + block._name)
	print_debug("Queue before: " + str(_queue))
	_queue.remove(block._queue_pos)
	print_debug("Queue after: " + str(_queue))
	# Check to see if a block is LIVE
	if (_liveNode.get_child_count() == 1):
		# Move this child to cancelled
		prevBlock = _liveNode.get_child(0)
		
		_cancel_block(prevBlock)
	
	# Show the live block
	block.show_block()
	
	#DEBUG
	print_debug("QueueNode before: " + str(_queueNode.get_child_count()))
		
	_move_live(block)
	
	#DEBUG
	print_debug("QueueNode after: " + str(_queueNode.get_child_count()))
	
	block.set_active(false)
	
	_end_turn()
	
	# Check queue status
	if _queueNode.get_child_count() == 0:
			GameManager.emit_signal("queue_empty")
			
	GameManager.emit_signal("turn_end")
	
	#  Expression capability for effects
	_execute_effect(GameManager.card_list[block.iq -1].effect, 
					GameManager.card_list[block.iq - 1].target)
					
	
func _toggle_queue():
	for block in _queueNode.get_children():
		if (is_human && _isMyTurn):
			block.set_active(true)
		else:
			block.set_active(false)
		
func _end_turn():
	_isMyTurn = false
	GameManager.disconnect("play_block", self, "_on_play_block")
	_toggle_queue()
	
func get_live_block() -> int:
	var node = _liveNode.get_child(0)
	
	if node != null:
		return node.iq
	else:
		return 0

func add_points(more_points):
	_points += more_points
	
	_pointNode.get_node("MarginContainer/Label").text = print_points()

func print_points() -> String:
	if (_points == 0):
		return "No viewers"
		
	return _point_format % (_points / _point_scale)

func clear_blocks():
	#DEBUG
	print_debug("Clearing blocks...")
	print_debug("Queue before: " + str(_queue))
	print_debug("QueueNode before: " + str(_queueNode.get_child_count()))
	for block in _cancelledNode.get_children():
		_cancelledNode.remove_child(block)
	for block in _liveNode.get_children():
		_liveNode.remove_child(block)
	_clear_queue()
	#DEBUG
	print_debug("Queue after: " + str(_queue))
	print_debug("QueueNode after: " + str(_queueNode.get_child_count()))

func reset_flags():
	_isMyTurn = false
	force_play_check = false
	block_effects = false
	block_alithia = false
	block_count = 0
	_points = 0
	
	# Unsubscribe to signals
	GameManager.disconnect("turn_start", self, "_on_turn_start")
	GameManager.disconnect("round_end", self, "_on_round_end")
	
func _clear_queue():
	for block in _queueNode.get_children():
		_queueNode.remove_child(block)
		_queue.remove(block._queue_pos)
	
	#Debug info
	assert(_queue.size() == 0)
	assert(_queueNode.get_child_count() == 0)
		
func _execute_effect(effect, target):
		GameManager.emit_signal(effect, target)	

func _on_round_end():
	_queue = []
	
func _move_live(block):
	_queueNode.remove_child(block)
	# Set this block to LIVE
	# Modify the visuals on the block
	block.size_flags_horizontal = SIZE_EXPAND_FILL
	block.size_flags_vertical = SIZE_EXPAND_FILL
	block.expand = true
	
	#disconnect from  signals
	disconnect("show_blocks", block, "_on_show")
	disconnect("hide_blocks", block, "_on_hide")
	
	_liveNode.add_child(block)
	
func _cancel_block(block):
	_liveNode.remove_child(block)
	# Modify the visuals on the block
	block.size_flags_horizontal = SIZE_FILL
	block.size_flags_vertical = SIZE_FILL
	block.expand = false
		
	_cancelledNode.add_child(block)
	
	# Check if cancelled block is TWiC
	# reset the force play check
	if (block.iq == 7):
		force_play_check = false
