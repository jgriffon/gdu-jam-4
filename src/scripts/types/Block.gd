extends TextureButton
class_name Block

export var iq: int = 1
var showFront = false

var _name
var _description
var _active = false
var _queue_pos:int = -1

# Called when the node enters the scene tree for the first time.
func _ready():
	# load in the name and description
	# from the card list
	_name = GameManager.card_list[iq - 1].name
	_description = GameManager.card_list[iq - 1].description

	$IQ.text = String(iq)
	
	if(_active):
		print("ready active")
		$Active.show()
	
func set_active(active = false):
	print("set_active")
	if (!active):
		_active = false
		$Active.hide()
	else:
		_active = true
		$Active.show()

func hide_block():
	print("on_hide")
	$IQ.hide()
	
func show_block():
	print("on_show")
	$IQ.show()

func _on_Block_pressed():
	if (_active):
		GameManager.emit_signal("block_info", self)
