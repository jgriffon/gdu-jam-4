extends PanelContainer
class_name ModalDisplay

# Template to add simple click-to-hide 
# boilerplate
func _input(event):
	if !visible:
		return
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			var evLocal = make_input_local(event)
			if !Rect2(Vector2(0,0),rect_size).has_point(evLocal.position):
				_on_hide_display()
				hide()

func _on_hide_display():
	pass
