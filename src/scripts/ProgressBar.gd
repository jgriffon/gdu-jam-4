extends ProgressBar


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	value = 50

func set_balance(players, point_goal):
	var ratio1 = players[0]._points / point_goal as float
	var ratio2 = players[1]._points / point_goal as float
	
	# Position player one past the midway point
	value = (ratio1 * 50) + 50 - (ratio2 * 50)
