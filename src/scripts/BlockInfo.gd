extends ModalDisplay


# The block referenced by this info box
var _block: Block

# Called when the node enters the scene tree for the first time.
func _ready():
	GameManager.connect("block_info", self, "_on_block_info")


func _on_block_info(block: Block):
	_block = block
	
	$MarginContainer/Contents/Name.text = _block._name
	$MarginContainer/Contents/Description.text = _block._description + "\n\n"

	show()


func _on_Play_Button_pressed():
	GameManager.emit_signal("play_block", _block)
	_block = null
	hide()
