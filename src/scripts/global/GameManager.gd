extends Node

# Signals
signal game_start
signal round_start
signal turn_start
signal turn_end
signal round_end
signal game_end
signal game_win(player)
signal queue_empty
signal block_info(block)
signal play_block(block)
signal show_queue(player)


# Contains all the card info
var card_list = []

# Game-wide data
const points_goal = 130000
const total_cards = 18
var _titleScreen = preload("res://scenes/Title.tscn")
var _effects
var player_character: Player

# Contains all the cards in shorthand, for shuffling
var deck = []

func _ready():
	_effects = Effects.new()

### Master Card List
## Hardcoded for now, but needs to change
## TODO: Add data reading for card set
## Structure of Data:
## Card:
##    - Name
##    - IQ
##    - Description
##    - Quantity
######################################
func _load_cards():
	
	## Master card list
	card_list = [	
		{
			"name": "Alithia in Action\nwith Seamus O’Bleefe",
			"iq": 1,
			"description": "Starring a muckraking intern who’s not afraid to spy on the opposition. Choose a block except this one. Any players with that block in queue are on administrative leave.",
			"quantity": 4,
			"effect":"effect_alithia",
			"target":"0"
		},
		{
			"name": "Cheap Ad",
			"iq": 2,
			"description": "Pay the bills with no frills. In order to compete with another network, you get to see their queue.",
			"quantity": 3,
			"effect":"effect_show_queue",
			"target":"2"
		},
		{
			"name": "Flutter's War",
			"iq": 3,
			"description": "Soft-hitting corporate commentary on social media. You and an opponent compare queues. Lower IQ media company out for the round.",
			"quantity": 3,
			"effect":"effect_flutter",
			"target":"2"
		},
		{
			"name": "On the Ground\nwith Andy Astroturf",
			"iq": 4,
			"description": "Professional organiser. Builds up a grassroots campaign of viewership support. Their rallies protect you from any effects until next turn.",
			"quantity": 2,
			"effect":"effect_protect",
			"target":"1"
		},
		{
			"name": "The Six",
			"iq": 5,
			"description": "Round-table discussion with overpaid executives. Only on late night. Play board room politics and pick an opponent. They lose their entire queue.",
			"quantity": 2,
			"effect":"effect_six",
			"target":"2"
		},
		{
			"name": "120 Minutes",
			"iq": 6,
			"description": "Undercover exposé show carefully vetted by the corporate lawyers. They’ll never let you say anything that’ll get you in trouble! Protects against Alithia in Action.",
			"quantity": 2,
			"effect":"effect_protect_alithia",
			"target":"1"
		},
		{
			"name": "This Week in the Company",
			"iq": 7,
			"description": "Starring the VP. He’s always too busy to be caugh….er, meet with you. If 120 Minutes or The Six are in your queue, this must be put on the air immediately.",
			"quantity": 1,
			"effect":"effect_queue",
			"target":"1"
		},
		{
			"name": "Mucker Ericson Tonight",
			"iq": 8,
			"description": "Nightly talk show sponsored by ACE Enterprises. They’ve got MEGA influence. This block ends the round.",
			"quantity": 1,
			"effect":"effect_mucker",
			"target":"2"
		}
	]
	
	# Reset deck size
	deck = []
	
	## Populate the deck from the card list
	for card in card_list:
		for q in card.quantity:
			deck.append(card.iq)
			#Add the new signal
			if(card.effect != ""):
				_effects._add(card.effect, card.target)
				
			
	assert(deck.size() == total_cards)

func shuffle(cards):
	# make a carbon copy
	var carbon = cards.duplicate()
	var temp_deck = []
	var i = 0
	
	randomize()
	
	while carbon.size() > 0:
		i = randi()%carbon.size()
		temp_deck.append(carbon[i])
		carbon.remove(i)
	
	assert(temp_deck.size() == total_cards)
	
	return temp_deck

func display_text(text):
	_effects._game_impl.display_text(text)
