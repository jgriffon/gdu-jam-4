extends ModalDisplay

func show_blocks(player:Player):
	get_tree().paused = true
	$Margin/Contents/Name.text = player.display_name
	
	# Clone block object and attach it to the Grid
	for block in player._queue:
		var newBlock = player._block.instance()
		newBlock.iq = block

		$Margin/Contents/Center/Blocks.add_child(newBlock)

	show()

# Called anytime the modal is hidden
func _on_hide_display():
	get_tree().paused = false
	for block in $Margin/Contents/Center/Blocks.get_children():
		$Margin/Contents/Center/Blocks.remove_child(block)
