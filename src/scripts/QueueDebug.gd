extends Label

var tree


# Called when the node enters the scene tree for the first time.
func _ready():
	GameManager.connect("game_start", self, "_update_player")
	GameManager.connect("game_start", self, "_update_player")
	GameManager.connect("round_start", self, "_update_player")
	GameManager.connect("round_end", self, "_update_player")
	GameManager.connect("turn_start", self, "_update_player")
	GameManager.connect("turn_end", self, "_update_player")
	GameManager.connect("play_block", self, "_update_player")
	tree = get_parent().get_parent().get_parent().get_parent()
	
func _update_player():
	# Get the current player
	var player: Player
	
	if (tree.currentPlayer != null):
		player = tree.players[tree.currentPlayer]

	if (player != null):
		text = "Queue: " + str(player._queue.size())
