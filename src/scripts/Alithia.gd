extends PanelContainer


signal player_info(player)
var _player: Player
var _pressed: Block
var _block = preload("res://scenes/Block.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	var _impl = GameManager._effects._game_impl
	_impl.get_tree().paused = true
	
	connect("player_info", self, "_on_player_info")
	# Populate the grid with the available blocks
	# Iterate through the block list
	for block in GameManager.card_list:
		if("Alithia" in block.name):
			continue

		# Instance a new block
		var display = _block.instance()
		display.iq = block.iq
		$Margin/Contents/Blocks.add_child(display)
		
		# Subscribe to the on click
		display.connect("pressed", self, "_on_pressed", [display])
	
func _on_player_info(player: Player):
	$Margin/Contents/Name.text = player.display_name
	_player = player

func _on_pressed(block: Block):
	block.get_node("Active").show()
	
	# Ensure only one block appears selected at a time
	if(_pressed != null):
		_pressed.get_node("Active").hide()
	
	_pressed = block
	$Margin/Contents/Confirm.disabled = false


func _on_Confirm_pressed():
	# Check to see if the opponent's queue contains this block!
	var _impl = GameManager._effects._game_impl
	
	if(_player._queue.find(_pressed.iq) > -1):
		# We have a match!
		# Make the current player win
		_impl._force_winner = _impl.players[_impl.currentPlayer]
		
		hide()
		# Tell the player what's happening!
		GameManager.display_text( 
			"Seamus found out what block they're using!")
			
		# Resume game
		GameManager.emit_signal("round_end")
	else:
		hide()
		# Tell the player what's happening!
		GameManager.display_text( 
			"Seamus went lurking in the wrong office...")
	
	_impl.get_tree().paused = false	

	queue_free()
	
