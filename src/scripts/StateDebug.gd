extends Label

# Called when the node enters the scene tree for the first time.
func _ready():
	GameManager.connect("game_start", self, "_game_start")
	GameManager.connect("game_end", self, "_game_end")
	GameManager.connect("round_start", self, "_round_start")
	GameManager.connect("round_end", self, "_round_end")
	GameManager.connect("turn_start", self, "_turn_start")
	GameManager.connect("turn_end", self, "_turn_end")
	GameManager.connect("play_block", self, "_play_block")
	GameManager.connect("block_info", self, "_block_info")
	GameManager.connect("game_win", self, "_game_win")
	GameManager.connect("queue_empty", self, "_qempty")
	GameManager.connect("show_queue", self, "_showq")
	
func _game_start():
	text = "Game::Start"
	
func _game_end():
	text = "Game::End"
	
func _game_win(player):
	text = "Game::Win"	
	
func _round_start():
	text = "Round::Start"
	
func _round_end():
	text = "Round::End"

func _turn_start():
	text = "Turn::Start"
		
func _turn_end():
	text = "Turn::End"
	
func _play_block():
	text = "Block::Play"

func _block_info(block):
	text = "Block::Info"

func _qempty():
	text = "Queue::Empty"
	
func _showq():
	text = "Queue::Show"
