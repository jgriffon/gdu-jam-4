extends Label

var tree

func _ready():
	GameManager.connect("turn_start", self, "_on_turn")
	tree = get_parent().get_parent().get_parent().get_parent()
	
func _on_turn():
	# Get the current player
	var player: Player
	
	if (tree.currentPlayer != null):
		player = tree.players[tree.currentPlayer]
	
	text = player.display_name
