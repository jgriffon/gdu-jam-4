#!/bin/sh
base=$1
convert "$base" -resize '29x29'     -unsharp 1x4 "Icon-Small.png"
convert "$base" -resize '40x40'     -unsharp 1x4 "Icon-Small-40.png"
convert "$base" -resize '50x50'     -unsharp 1x4 "Icon-Small-50.png"
convert "$base" -resize '57x57'     -unsharp 1x4 "Icon.png"
convert "$base" -resize '58x58'     -unsharp 1x4 "Icon-Small@2x.png"
convert "$base" -resize '60x60'     -unsharp 1x4 "Icon-60.png"
convert "$base" -resize '72x72'     -unsharp 1x4 "Icon-72.png"
convert "$base" -resize '76x76'     -unsharp 1x4 "Icon-76.png"
convert "$base" -resize '80x80'     -unsharp 1x4 "Icon-Small-40@2x.png"
convert "$base" -resize '100x100'   -unsharp 1x4 "Icon-Small-50@2x.png"
convert "$base" -resize '114x114'   -unsharp 1x4 "Icon@2x.png"
convert "$base" -resize '120x120'   -unsharp 1x4 "Icon-60@2x.png"
convert "$base" -resize '144x144'   -unsharp 1x4 "Icon-72@2x.png"
convert "$base" -resize '152x152'   -unsharp 1x4 "Icon-76@2x.png"
convert "$base" -resize '180x180'   -unsharp 1x4 "Icon-60@3x.png"
convert "$base" -resize '512x512'   -unsharp 1x4 "iTunesArtwork"
convert "$base" -resize '1024x1024' -unsharp 1x4 "iTunesArtwork@2x"

convert "$base" -resize '36x36'     -unsharp 1x4 "Icon-ldpi.png"
convert "$base" -resize '48x48'     -unsharp 1x4 "Icon-mdpi.png"
convert "$base" -resize '72x72'     -unsharp 1x4 "Icon-hdpi.png"
convert "$base" -resize '96x96'     -unsharp 1x4 "Icon-xhdpi.png"
convert "$base" -resize '144x144'   -unsharp 1x4 "Icon-xxhdpi.png"
convert "$base" -resize '192x192'   -unsharp 1x4 "Icon-xxxhdpi.png"

# WIndows 
convert "$base" -define icon:auto-resize=256,128,64,48,32,16 icon.ico

# Mac OS X
convert "$base" -resize '1024x1024' -unsharp 1x4 "icns-1024px.png"
convert "$base" -resize '512x512' -unsharp 1x4 "icns-512px.png"
convert "$base" -resize '256x256' -unsharp 1x4 "icns-256px.png"
convert "$base" -resize '128x128' -unsharp 1x4 "icns-128px.png"
#convert "$base" -resize '64x64' -unsharp 1x4 "icns-64px.png"
convert "$base" -resize '32x32' -unsharp 1x4 "icns-32px.png"
convert "$base" -resize '16x16' -unsharp 1x4 "icns-16px.png"

# iOS Launhc Screens
## Landscape
convert "launch.png" -resize '2436x1125!'  "launch-2436x1125.png"
convert "launch.png" -resize '2208x1242!'  "launch-2208x1242.png"
convert "launch.png" -resize '1024x768!'  "launch-1024x768.png"
convert "launch.png" -resize '2048x1536!'  "launch-2048x1536.png"
## Portrait
convert "launch.png" -resize '640x960!'  "launch-640x960.png"
convert "launch.png" -resize '640x1136!'  "launch-640x1136.png"
convert "launch.png" -resize '750x1334!' "launch-750x1334.png"
convert "launch.png" -resize '1125x2436!'  "launch-1125x2436.png"
convert "launch.png" -resize '768x1024!'  "launch-768x1024.png"
convert "launch.png" -resize '1536x2048!'  "launch-1536x2048.png"
convert "launch.png" -resize '1242x2208!'  "launch-1242x2208.png"

 

png2icns icon.icns icns-*px.png
rm icns-*px.png
